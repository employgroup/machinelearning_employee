﻿using CsvHelper.Configuration;
using CSVHelperProject.Models;

namespace CSVHelperProject.Mappers
{
    public sealed class EmployeeMap : ClassMap<EmployeeModel>
    {
        public EmployeeMap()
        {
            Map(m => m.Department).Name(Constants.CsvHeaders.Department);
            Map(m => m.EmployeeFirstName).Name(Constants.CsvHeaders.EmployeeFirstName);
            Map(m => m.EmployeeLastName).Name(Constants.CsvHeaders.EmployeeLastName);
            Map(m => m.AwardLevel).Name(Constants.CsvHeaders.AwardLevel);
            Map(m => m.AwardLevelRate).Name(Constants.CsvHeaders.AwardLevelRate);
            Map(m => m.PredictedAwardLevel).Name(Constants.CsvHeaders.PredictedAwardLevel);
            Map(m => m.PredictedAwardRate).Name(Constants.CsvHeaders.PredictedAwardRate);
            Map(m => m.DOJ).Name(Constants.CsvHeaders.DOJ);
            Map(m => m.Gender).Name(Constants.CsvHeaders.Gender);
            Map(m => m.Age).Name(Constants.CsvHeaders.Age); 
            Map(m => m.Tenure).Name(Constants.CsvHeaders.Tenure);
            Map(m => m.Position).Name(Constants.CsvHeaders.Position);
            Map(m => m.TimeInPosition).Name(Constants.CsvHeaders.TimeInPosition);
        }

   

    }
}
