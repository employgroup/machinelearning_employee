﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReadytechWorkforceML.Business;
using ReadytechWorkforceML.Models;

namespace ReadytechWorkforceML.Controllers
{
    public class DashboardController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public DashboardController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(new PredictionBusiness().GetResults());
        }

        public IActionResult Employees()
        {
            return View(new PredictionBusiness().GetResults());
        }

        public IActionResult Index2()
        {
            IResultsAnalyser analyser = new ResultsAnalyser(new PredictionBusiness().GetResults().Data);
            
            return View(new DashboardViewModel()
            {
                GenderAnalysis = analyser.GetGenderAnalysis(),
                DepartmentAnalysis = analyser.GetDepartmentAnalysis(),
            });
        }
    }
}
