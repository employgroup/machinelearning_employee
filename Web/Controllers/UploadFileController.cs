﻿using System.Threading.Tasks;
using FileUploadExample.Models;
using Microsoft.AspNetCore.Mvc;
using CSVHelperProject.Services;
using ReadytechWorkforceML.Business;

namespace FileUploadExample.Controllers
{
    public class UploadFileController : Controller
    {
   
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(InputFile item)
        {
            PredictionBusiness prediction = new PredictionBusiness();
            var result = prediction.ProcessData(item.CsvFile);
            if(result)
            {
                return RedirectToAction("Index", "Dashboard");
            }
           

            return RedirectToAction("Index", "Home");
            
 
        }
    }
}