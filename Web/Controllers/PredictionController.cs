﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReadytechWorkforceML.Business;
using ReadytechWorkforceML.Models;
using ReadytechWorkforceML.Models.Response;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ReadytechWorkforceML.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class PredictionController : ControllerBase
    {
      
        // GET: api/<PredictionController>
        [HttpGet]
        public PredictionResult GetResults()
        {

            IPredictionBusiness prediction = new PredictionBusiness();
            return prediction.GetResults();
        }


        //// POST api/<PredictionController>
        //[HttpPost]
        //public void Post([FromBody] InputFile value)
        //{

        //    var filedata = value;
        //}


    }
}
