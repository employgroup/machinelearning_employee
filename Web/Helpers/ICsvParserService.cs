﻿using CSVHelperProject.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;

namespace CSVHelperProject.Services
{
    public interface ICsvParserService
    {
        List<EmployeeModel> ReadCsvFileToEmployeeModel(IFormFile file);


    }
}
