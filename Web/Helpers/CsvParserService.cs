﻿using CsvHelper;
using CSVHelperProject.Mappers;
using CSVHelperProject.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace CSVHelperProject.Services
{
    public class CsvParserService : ICsvParserService
        {
            public List<EmployeeModel> ReadCsvFileToEmployeeModel(IFormFile file)
            {
                try
                {
                using   (var reader = new StreamReader(file.OpenReadStream()))
                    using (var csv = new CsvReader(reader,CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.RegisterClassMap<EmployeeMap>();
                        csv.Configuration.BadDataFound = null;
                        csv.Configuration.HeaderValidated = null;
                        csv.Configuration.MissingFieldFound = null;
                    var records = csv.GetRecords<EmployeeModel>().ToList();
                        return records;
                    }
                }
                catch (UnauthorizedAccessException e)
                {
                    throw new Exception(e.Message);
                }
                catch (FieldValidationException e)
                {
                    throw new Exception(e.Message);
                }
                catch (CsvHelperException e)
                {
                    throw new Exception(e.Message);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
 
    }
}

