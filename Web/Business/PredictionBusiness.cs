﻿using CSVHelperProject.Models;
using CSVHelperProject.Services;
using Microsoft.AspNetCore.Http;
using ReadytechWorkforceML.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ML_Model;
using System.Security.Cryptography.X509Certificates;

namespace ReadytechWorkforceML.Business
{
    public class PredictionBusiness: IPredictionBusiness
    {

        static PredictionResult EmplDashBoard;

        public PredictionResult GetResults()
        {
            if (EmplDashBoard == null)
            {
                EmplDashBoard = GetMockupData();
            }
            
            // get top ten departments
            var deparments = EmplDashBoard.Data
                .GroupBy(x => x.Department)
                .OrderByDescending(x => x.Count())
                .Take(10).Select(x => x.Key).ToList();
            
            var departmentsData = new List<PredictionResultItem>();
            foreach (var department in deparments)
            {
                departmentsData.AddRange(EmplDashBoard.Data.Where(x => x.Department == department).ToList());
            }

            EmplDashBoard.Data = departmentsData;
            return EmplDashBoard;
        }

        private PredictionResult GetMockupData()
        {
            PredictionResult result = new PredictionResult { Data = new List<PredictionResultItem>() };
            result.Data.Add(new PredictionResultItem() { Age = "45", Award = "Supervisor", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "ABC", DOJ = "2001", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1001", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 30.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "45", Award = "supervisor", AwardLevel = "Level2", AwardLevelRate = 25.34, Department = "ABC", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1003", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "25", Award = "front office", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "B", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "22", Award = "accountant", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "B", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "65", Award = "chef", AwardLevel = "Level3", AwardLevelRate = 25.34, Department = "A", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "19", Award = "room service", AwardLevel = "Level3", AwardLevelRate = 25.34, Department = "HouseKeeping", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "65", Award = "Supervisor", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "HouseKeeping", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "49", Award = "manager", AwardLevel = "Level2", AwardLevelRate = 25.34, Department = "C", DOJ = "1992", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "28", Award = "security", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "HouseKeeping", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "45", Award = "security", AwardLevel = "Level3", AwardLevelRate = 25.34, Department = "HouseKeeping", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "45", Award = "chef", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "HouseKeeping", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Male", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "", PredictedDiffPercentage = 2, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });

            result.Data.Add(new PredictionResultItem() { Age = "37", Award = "manager", AwardLevel = "Level3", AwardLevelRate = 25.34, Department = "C", DOJ = "1980", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",           PredictedDiffPercentage = -1, PredictedLevelRate = 14.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "33", Award = "sweaper", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "C", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",           PredictedDiffPercentage = -1, PredictedLevelRate = 14.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "27", Award = "sweaper", AwardLevel = "Level2", AwardLevelRate = 25.34, Department = "C", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",           PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "55", Award = "room service ", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "C", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",     PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "33", Award = "accountant", AwardLevel = "Level3", AwardLevelRate = 25.34, Department = "A", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",        PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "25", Award = "chef", AwardLevel = "Level1", AwardLevelRate = 25.34, Department = "HouseKeeping", DOJ = "2002", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",   PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "41", Award = "trainor", AwardLevel = "Level2", AwardLevelRate = 25.34, Department = "ABC", DOJ = "1999", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1002", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",         PredictedDiffPercentage = 27, PredictedLevelRate = 44.20, Tenure = "10", TimeInPosition = "15" });
            result.Data.Add(new PredictionResultItem() { Age = "55", Award = "trainor", AwardLevel = "Level2", AwardLevelRate = 25.34, Department = "A", DOJ = "2012", EmployeeFirstName = "jack", EmployeeLastName = "Jill", EmployeeUID = "1000", Gender = "Female", Location = "Vic", Position = "Kitchen-Housekeeping", PredictedAwardLevel = "",           PredictedDiffPercentage = 27, PredictedLevelRate = 24.20, Tenure = "10", TimeInPosition = "15" });

            return result;
        }

        private string GenerateName(int len)
        {
            Random r = new Random();
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
            string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
            string Name = "";
            Name += consonants[r.Next(consonants.Length)].ToUpper();
            Name += vowels[r.Next(vowels.Length)];
            int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
            while (b < len)
            {
                Name += consonants[r.Next(consonants.Length)];
                b++;
                Name += vowels[r.Next(vowels.Length)];
                b++;
            }

            return Name;


        }

        public bool ProcessData(IFormFile file)
        {
            var csvParserService = new CsvParserService();
            var result = csvParserService.ReadCsvFileToEmployeeModel(file);
            EmplDashBoard = new PredictionResult { Data = new List<PredictionResultItem>() };

            foreach (EmployeeModel emp in result)
            {
                var rateAward = SalaryPrediction.GetRate(emp.Department, int.Parse(emp.Age),int.Parse( emp.DOJ), emp.Gender,emp.Position,int.Parse( emp.Tenure), int.Parse(emp.TimeInPosition));
                try
                {
                    var rate = rateAward.Key;
                    var award = rateAward.Value;
                    EmplDashBoard.Data.Add(new PredictionResultItem()
                    {
                        Age = emp.Age,
                        Award = "",
                        AwardLevel = emp.AwardLevel,
                        AwardLevelRate = double.Parse(emp.AwardLevelRate),
                        Department = emp.Department,
                        DOJ = emp.DOJ,
                        EmployeeFirstName = GenerateName(3),
                        EmployeeLastName = GenerateName(6),
                        EmployeeUID = "",
                        Gender = emp.Gender,
                        Position = emp.Position,
                        PredictedAwardLevel = award,
                        PredictedDiffPercentage = (rate - double.Parse(emp.AwardLevelRate)) / double.Parse(emp.AwardLevelRate) * 1.0 * 100,
                        PredictedLevelRate = rate,
                        Tenure = emp.Tenure,
                        TimeInPosition = emp.TimeInPosition
                    });
                }
                catch (Exception ex)
                {
                 
                }
            }


            return true;

        }
    }
}
