﻿using ReadytechWorkforceML.Extensions;
using ReadytechWorkforceML.Models;
using ReadytechWorkforceML.Models.Response;
using System.Collections.Generic;
using System.Linq;

namespace ReadytechWorkforceML.Business
{
    public class ResultsAnalyser : IResultsAnalyser
    {
        readonly List<PredictionResultItem> _data;

        public ResultsAnalyser(List<PredictionResultItem> data)
        {
            _data = data;
        }

        public List<DepartmentAnalysisViewModel> GetDepartmentAnalysis()
        {

            var list = _data.GroupBy(x => x.Department)
                .Select(x => new DepartmentAnalysisViewModel()
                {
                    Department = x.Key,
                    Ok = _data.Count(j => j.Department == x.Key && PredictionResultExtensions.IsOk(j)),
                    Underpaid = _data.Count(k => k.Department == x.Key && PredictionResultExtensions.IsUnderpaid(k)),
                })
                .OrderBy(x=>x.Department).ToList();

            return list;
        }

        public GenderAnalysisViewModel GetGenderAnalysis()
        {
            return new GenderAnalysisViewModel()
            {
                MaleOk = _data.Where(x => PredictionResultExtensions.IsMale(x) && PredictionResultExtensions.IsOk(x)).Count(),
                FemaleOk = _data.Where(x => PredictionResultExtensions.IsFemale(x) && PredictionResultExtensions.IsOk(x)).Count(),
                MaleUnderpaid = _data.Where(x => PredictionResultExtensions.IsMale(x) && PredictionResultExtensions.IsUnderpaid(x)).Count(),
                FemaleUnderpaid = _data.Where(x => PredictionResultExtensions.IsFemale(x) && PredictionResultExtensions.IsUnderpaid(x)).Count(),
            };
        }
    }
}
