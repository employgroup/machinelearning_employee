﻿using ReadytechWorkforceML.Models;
using System.Collections.Generic;

namespace ReadytechWorkforceML.Business
{
    interface IResultsAnalyser
    {
        GenderAnalysisViewModel GetGenderAnalysis();
        List<DepartmentAnalysisViewModel> GetDepartmentAnalysis();
    }
}
