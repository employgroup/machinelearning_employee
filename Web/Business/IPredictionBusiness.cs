﻿using ReadytechWorkforceML.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadytechWorkforceML.Business
{
    interface IPredictionBusiness
    {
        PredictionResult GetResults();
    }
}
