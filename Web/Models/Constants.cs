﻿namespace CSVHelperProject
{
    public static class Constants
    {
        public class CsvHeaders
        {
           
            internal const string EmployeeFirstName= "EmployeeFirstName";
            internal const string Department = "Department";
            internal const string AwardLevelRate = "AwardLevelRate";
            internal const string TimeInPosition = "TimeInPosition";
            internal const string Position = "Position";
            internal const string Tenure = "Tenure";
            internal const string Gender = "Gender";
            internal const string Age = "Age";
            internal const string PredictedAwardRate = "PredictedAwardRate";
            internal const string PredictedAwardLevel = "PredictedAwardLevel";
            internal const string AwardLevel = "AwardLevel";
            internal const string EmployeeLastName = "EmployeeLastName";
            internal const string DOJ = "DOJ";
        }
    }
}
