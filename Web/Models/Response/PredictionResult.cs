﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReadytechWorkforceML.Models.Response
{ 
    public class PredictionResultItem
    {
        public string MLDataUID { get; set; }
        public string Department { get; set; }
        public string EmployeeUID { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }
        public string Award { get; set; }
        public string AwardLevel { get; set; }
        public string PredictedAwardLevel { get; set; }
        public double AwardLevelRate { get; set; }
        public double PredictedLevelRate { get; set; }
        public double PredictedDiffPercentage { get; set; }
        public string DOJ { get; set; }
        public string Location { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string Tenure { get; set; }
        public string Position { get; set; }
        public string TimeInPosition { get; set; }
    }

    public class PredictionResult
    {
        public List<PredictionResultItem> Data { get; set; }
    }

}
