﻿namespace ReadytechWorkforceML.Models
{
    public class GenderAnalysisViewModel
    {
        public int MaleOk { get; set; }
        public int FemaleOk { get; set; }
        public int MaleUnderpaid { get; set; }
        public int FemaleUnderpaid { get; set; }
    }
}
