﻿namespace ReadytechWorkforceML.Models
{
    public class DepartmentAnalysisViewModel
    {
        public string Department { get; set; }
        public int Ok { get; set; }
        public int Underpaid { get; set; }
    }
}
