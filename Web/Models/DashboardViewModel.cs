using System;
using System.Collections.Generic;

namespace ReadytechWorkforceML.Models
{
    public class DashboardViewModel
    {
        public GenderAnalysisViewModel GenderAnalysis { get; set; }
        public List<DepartmentAnalysisViewModel> DepartmentAnalysis { get; set; }
    }
}
