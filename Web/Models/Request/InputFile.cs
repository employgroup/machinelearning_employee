﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadExample.Models
{
    public class InputFile
    {
        [Key]
        public int Id { get; set; }
 
        /// <summary>
        /// The photo file upload
        /// </summary>
        [NotMapped] //Tell Entity Framework to ignore property
        public IFormFile CsvFile { get; set; }
    }
}
