﻿using CsvHelper.Configuration.Attributes;

namespace CSVHelperProject.Models
{
    public class EmployeeModel
    {
        [Name(Constants.CsvHeaders.Department)]
        public string Department { get; set; }


        [Name(Constants.CsvHeaders.EmployeeFirstName)]
        public string EmployeeFirstName { get; set; }

        [Name(Constants.CsvHeaders.EmployeeLastName)]
        public string EmployeeLastName { get; set; }

        [Name(Constants.CsvHeaders.AwardLevel)]
        public string AwardLevel { get; set; }

        [Name(Constants.CsvHeaders.AwardLevelRate)]
        public string AwardLevelRate { get; set; }

        [Name(Constants.CsvHeaders.PredictedAwardLevel)]
        public string PredictedAwardLevel { get; set; }

        [Name(Constants.CsvHeaders.PredictedAwardRate)]
        public string PredictedAwardRate { get; set; }

        [Name(Constants.CsvHeaders.DOJ)]
        public string DOJ { get; set; }

        [Name(Constants.CsvHeaders.Gender)]
        public string Gender { get; set; }

        [Name(Constants.CsvHeaders.Age)]
        public string Age { get; set; }


        [Name(Constants.CsvHeaders.Tenure)]
        public string Tenure { get; set; }


        [Name(Constants.CsvHeaders.Position)]
        public string Position { get; set; }

        [Name(Constants.CsvHeaders.TimeInPosition)]
        public string TimeInPosition { get; set; }
 
    }
}
