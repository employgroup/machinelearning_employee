﻿using ReadytechWorkforceML.Models.Response;

namespace ReadytechWorkforceML.Extensions
{
    public static class PredictionResultExtensions
    {
        public static bool IsMale(this PredictionResultItem item)
        {
            return item.Gender == "Male";
        }

        public static bool IsFemale(this PredictionResultItem item)
        {
            return item.Gender == "Female";
        }

        public static bool IsOk(this PredictionResultItem item)
        {
            return item.AwardLevelRate >= item.PredictedLevelRate;
        }

        public static bool IsUnderpaid(this PredictionResultItem item)
        {
            return item.AwardLevelRate < item.PredictedLevelRate;
        }
    }
}
