﻿using ML_Model;
using System;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var rate= SalaryPrediction.GetRate("STC", 45, 2015, "Male", "Support Worker", 1704, 1704);
            Console.WriteLine($"Current Rate - { rate}");

        }
    }
}
