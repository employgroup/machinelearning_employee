﻿using System.Reflection;

namespace ML_Model
{
    internal class AwardCode
    {
        public string Award { get; set; }
        public float Min { get; set; }
        public float Max { get; set; }
    }
}