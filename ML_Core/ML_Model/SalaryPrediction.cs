using Microsoft.ML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ML_Model
{
    public class SalaryPrediction
    {
        private static Lazy<PredictionEngine<ModelInput, ModelOutput>> PredictionEngine = new Lazy<PredictionEngine<ModelInput, ModelOutput>>(CreatePredictionEngine);

        private static List<AwardCode> awardCodes;

        public static string MLNetModelPath = Path.GetFullPath("MLModel.zip");
        public static string AwardPath = Path.GetFullPath("SCHADS_Award_Rates.csv");

        public static KeyValuePair<float,string> GetRate(string department, int age,
               int DOJ, string gender,  string position, int tenure, int timeInPosition)
        {
            ModelInput input = new ModelInput()
            {
                Age = age,
                Department = department,
                DOJ = DOJ,
                Gender = gender,
                Position = position,
                Tenure = tenure,
                TimeInPosition = timeInPosition
            };
            var rate = Predict(input).Score;
            var award = awardCodes.FirstOrDefault(a => rate < a.Max && rate > a.Min)?.Award;

            return new KeyValuePair<float, string>(rate,award);
        }
        private static ModelOutput Predict(ModelInput input)
        {
            ModelOutput result = PredictionEngine.Value.Predict(input);
            return result;
        }

        public static PredictionEngine<ModelInput, ModelOutput> CreatePredictionEngine()
        {
             awardCodes = File.ReadAllLines(AwardPath).Skip(1)
            .Select(line => line.Split(new char[] { ',' }))
            .Select(i =>
              new AwardCode
              {
                  Award = i[0],
                  Max = float.Parse(i[2]),
                  Min = float.Parse(i[1])
              }
            ).ToList();


            // Create new MLContext
            MLContext mlContext = new MLContext();

            // Load model & create prediction engine
            ITransformer mlModel = mlContext.Model.Load(MLNetModelPath, out var modelInputSchema);
            var predEngine = mlContext.Model.CreatePredictionEngine<ModelInput, ModelOutput>(mlModel);

            return predEngine;
        }
    }
}
